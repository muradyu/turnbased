TurnBased
====================

Made with Unity 2020.3.2f1
Startup scene is "Battlefield" 

Controls:
    - Hold right mouse button to orbit camera
    - Green hex selection shows a valid movement position for a current fighter and a red one indicates an unreachable target
    - White hex selection shows a current active fighter
    - Click an empty hex to move
    - Click an enemy within a current fighter's movement range - your unit will move along a path and strike
    - Fighters with "StrikeAndReturn" ability will move along path, attack, and return to a start location
    - Press Skip button to move control to a next fighter

Bots will attack a closest enemy and fighters with low health and a low level of "fortitude" will flee.
Spawn positions are customizable through the "Spawner" object.
Fighter properties are randomized (based on default props + some random values)
The HexGrid size is customizable (You have to rebake GridNavigation after changing the grid size).


Some notes about the project:
IMGUI is used as fast UI prototyping tool.
LINQ functions could be slow and tend to generate garbage, but could be replaced with non-allocating custom implementations.
Object pooling could also be used to avoid heap allocations.
Pathfinding (as a heavy task), should be implemented as a Job or using Threads to avoid blocking main thread.
