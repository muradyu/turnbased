using System;
using UnityEngine;

public abstract class AITask : ScriptableObject
{
    [SerializeField] protected int priority;
    [SerializeField] protected AICondition[] runConditions;
    [NonSerialized] private AICondition[] conditions;
    [NonSerialized] protected Battler bot;
    [NonSerialized] protected AIInput input;

    public int Priority => priority;

    public void Construct(AIInput input)
    {
        this.input = input;
        conditions = new AICondition[runConditions.Length];
        for (int i = 0; i < runConditions.Length; i++)
        {
            conditions[i] = Instantiate(runConditions[i]);
            conditions[i].Construct(input);
        }
    }

    public virtual void Initialize(Battler bot)
    {
        this.bot = bot;
        for (int i = 0; i < conditions.Length; i++)
        {
            conditions[i].Initialize(bot);
        }
    }

    public abstract ICommand DoTask();

    public bool CheckRunConditions()
    {
        for (int i = 0; i < conditions.Length; i++)
        {
            if (!conditions[i].CheckCondition())
            {
                return false;
            }
        }
        return true;
    }
}
