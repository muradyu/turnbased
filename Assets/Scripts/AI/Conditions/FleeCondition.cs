using UnityEngine;

[CreateAssetMenu(fileName = "FleeCondition", menuName = "AI/AIConditions/FleeCondition")]
public class FleeCondition : AICondition
{
    [SerializeField]
    private int healthToFlee;
    public override void Initialize(Battler bot)
    {
        base.Initialize(bot);
    }

    public override bool CheckCondition()
    {
        return bot.Properties.Fortitude == 0 && bot.Health <= healthToFlee;
    }
}
