using UnityEngine;

[CreateAssetMenu(fileName = "AttackEnemyCondition", menuName = "AI/AIConditions/AttackEnemyCondition")]
public class AttackEnemyCondition : AICondition
{
    public override void Initialize(Battler bot)
    {
        base.Initialize(bot);
    }

    public override bool CheckCondition()
    {
        var closest = input.GetClosestEnemyTo(bot);
        return input.Battle.CheckCanAttackMelee(bot, closest);
    }
}
