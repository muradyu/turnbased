using UnityEngine;

[CreateAssetMenu(fileName = "MoveAndAttackClosestCondition", menuName = "AI/AIConditions/MoveAndAttackClosestCondition")]
public class MoveAndAttackClosestCondition : AICondition
{
    public override void Initialize(Battler bot)
    {
        base.Initialize(bot);
    }

    public override bool CheckCondition()
    {
        var closest = input.GetClosestEnemyTo(bot);
        var path = input.GridNav.GetPathClamped(bot.Position, closest.Position, bot.Properties.MaxSteps);
        if(path.Status == NavPathStatus.PathInvalid)
            return false;
        var lastCorner = path.Corners[path.Corners.Length - 1];
        if (!input.Battle.TryGetBattlerAt(lastCorner, out var battler))
            return input.Battle.CheckCanAttackMelee(lastCorner, closest.Position);
        return battler.Id == closest.Id;
    }
}
