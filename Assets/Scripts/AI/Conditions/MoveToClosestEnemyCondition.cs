using UnityEngine;

[CreateAssetMenu(fileName = "MoveToClosestEnemyCondition", menuName = "AI/AIConditions/MoveToClosestEnemyCondition")]
public class MoveToClosestEnemyCondition : AICondition
{
    public override void Initialize(Battler bot)
    {
        base.Initialize(bot);
    }

    public override bool CheckCondition()
    {
        var closest = input.GetClosestEnemyTo(bot);
        var path = input.GridNav.GetPathClamped(bot.Position, closest.Position, bot.Properties.MaxSteps);
        if (path.Status == NavPathStatus.PathInvalid)
            return false;
        return true;
    }
}
