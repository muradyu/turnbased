using System;
using UnityEngine;

public abstract class AICondition : ScriptableObject
{
    [NonSerialized]
    protected Battler bot;

    [NonSerialized]
    protected AIInput input;

    public void Construct(AIInput input)
    {
        this.input = input;
    }

    public virtual void Initialize(Battler bot)
    {
        this.bot = bot;
    }

    public abstract bool CheckCondition();
}
