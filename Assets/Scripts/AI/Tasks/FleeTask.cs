using UnityEngine;

[CreateAssetMenu(fileName = "FleeTask", menuName = "AI/AITasks/FleeTask")]
public class FleeTask : AITask
{
    public override void Initialize(Battler bot)
    {
        base.Initialize(bot);
    }

    public override ICommand DoTask()
    {
        var fleePos = input.Grid;
        var currentCell = input.Grid.WorldPosToCell(bot.Position);
        var newCell = GetRandomCell(currentCell);
        var pathStart = input.Grid.CellToIndex(currentCell);
        var pathEnd = input.Grid.CellToIndex(newCell);
        var path = input.GridNav.GetPath(pathStart, pathEnd);
        return new MoveCommand(bot, path);
    }

    private Vector2Int GetRandomCell(Vector2Int currentCell)
    {
        var newCell = currentCell + GetRandomOffset(2, bot.Properties.MaxSteps);
        var valid = input.Grid.ValidateCell(newCell);
        if(valid && !input.Battle.TryGetBattlerAt(newCell, out var battler))
            return newCell;
        return GetRandomCell(currentCell);
    }

    private Vector2Int GetRandomOffset(int minOffset, int maxOffset)
    {
        var rndX = Random.Range(minOffset, maxOffset) * RandomExt.RandomSign();
        var rndY = Random.Range(minOffset, maxOffset) * RandomExt.RandomSign();
        return new Vector2Int(rndX, rndY);
    }
}
