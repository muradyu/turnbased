using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "MoveToClosestEnemyTask", menuName = "AI/AITasks/MoveToClosestEnemyTask")]
public class MoveToClosestEnemyTask : AITask
{
    public override void Initialize(Battler bot)
    {
        base.Initialize(bot);
    }

    public override ICommand DoTask()
    {
        var target = input.GetClosestEnemyTo(bot);
        var path = input.GridNav.GetPathClamped(bot.Position, target.Position, bot.Properties.MaxSteps);
        return new MoveCommand(bot, path);
    }
}
