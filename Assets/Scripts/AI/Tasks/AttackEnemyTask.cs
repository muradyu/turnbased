using UnityEngine;

[CreateAssetMenu(fileName = "AttackEnemyTask", menuName = "AI/AITasks/AttackEnemyTask")]
public class AttackEnemyTask : AITask
{
    public override void Initialize(Battler bot)
    {
        base.Initialize(bot);
    }

    public override ICommand DoTask()
    {
        var target = input.GetClosestEnemyTo(bot);
        return new AttackCommand(bot, target);
    }
}
