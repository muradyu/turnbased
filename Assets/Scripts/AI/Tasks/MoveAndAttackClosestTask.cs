using UnityEngine;

[CreateAssetMenu(fileName = "MoveAndAttackClosestTask", menuName = "AI/AITasks/MoveAndAttackClosestTask")]
public class MoveAndAttackClosestTask : AITask
{
    public override void Initialize(Battler bot)
    {
        base.Initialize(bot);
    }

    public override ICommand DoTask()
    {
        var target = input.GetClosestEnemyTo(bot);
        var path = input.GridNav.GetPathClamped(bot.Position, target.Position, bot.Properties.MaxSteps);
        var set = new CommandSet();
        set.Append(new MoveCommand(bot, path));
        set.Append(new AttackCommand(bot, target));

        if (bot.Properties.StrikeAndReturn)
        {
            var reversed = input.GridNav.GetPathReversed(path);
            set.Append(new MoveCommand(bot, reversed));
        }
        return set;
    }
}
