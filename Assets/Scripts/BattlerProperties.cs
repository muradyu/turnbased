using System;
using UnityEngine;

[Serializable]
public class BattlerProperties
{
    [SerializeField]
    private int maxHealth;

    [SerializeField]
    private int maxSteps;

    [SerializeField]
    private int damageAmount;

    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private int fortitude;

    [SerializeField]
    private bool strikeAndReturn;

    public int MaxHealth => maxHealth;
    public int MaxSteps => maxSteps;
    public int DamageAmount => damageAmount;
    public float MoveSpeed => moveSpeed;
    public int Fortitude => fortitude;
    public bool StrikeAndReturn => strikeAndReturn;

    public BattlerProperties(int health, int steps, int damage, float speed, int fortitude, bool returnable)
    {
        this.maxHealth = health;
        this.maxSteps = steps;
        this.damageAmount = damage;
        this.moveSpeed = speed;
        this.fortitude = fortitude;
        this.strikeAndReturn = returnable;
    }
}
