﻿using System;
using UnityEngine;

public class Battler
{
    private BattlerProperties properties;
    private ICommand current;

    public event Action<Battler> OnActivate;
    public event Action<Battler> OnDone;
    public event Action<Battler, Battler> OnAttack;
    public event Action<Battler, Vector3> OnMove;
    public event Action<Battler, int> OnDamage;
    public event Action<Battler> OnDied;
    public event Action<Battler> OnDestroy;

    private BattlerView view;
    private int id;
    private int teamId;
    private int health;

    public BattlerProperties Properties => properties;
    public Vector3 Position => view.PositionFlat();
    public int Health => health;
    public int Id => id;
    public int TeamId => teamId;
    public BattlerView View => view;

    public Battler(BattlerView view, int id, int team, BattlerProperties props)
    {
        this.view = view;
        this.id = id;
        this.teamId = team;
        this.properties = props;
        this.health = props.MaxHealth;
        view.Initialize(teamId, props.MoveSpeed);
    }

    public void Activate()
    {
        OnActivate?.Invoke(this);
    }

    public void AssignCommand(ICommand command)
    {
        this.current = command;
        command.Start();
    }

    public void Execute()
    {
        if (current == null)
            return;
        if (current.IsDone)
        {
            current = null;
            OnDone?.Invoke(this);
            view.FinishMovement();
            return;
        }
        current.Execute();
    }

    public void MoveTo(Vector3[] path)
    {
        view.StartMovement(path);
        OnMove?.Invoke(this, path[path.Length - 1]);
    }

    public void UpdateView()
    {
        view.UpdateTransform();
    }

    public void TakeDamage(int amount)
    {
        health -= amount;
        OnDamage?.Invoke(this, amount);
        if (health <= 0)
        {
            OnDied?.Invoke(this);
        }
    }

    public void Attack(Battler target)
    {
        OnAttack?.Invoke(this, target);
        target.TakeDamage(properties.DamageAmount);
    }

    public void Destroy()
    {
        OnDestroy?.Invoke(this);
        view.Destroy();
    }
}
