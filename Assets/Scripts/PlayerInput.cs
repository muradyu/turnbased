using UnityEngine;
using Zenject;

public class PlayerInput : ITickable
{
    private Battle battle;
    private Plane plane;
    private Camera camera;
    private HexGrid grid;
    private GridNavigation gridNav;
    private Vector3 lastHitPos;
    private Vector2Int lastCell;

    public Vector3 LastHitPos => lastHitPos;
    public bool IsActive => battle.CurrentBattler != null && battle.CurrentBattler.TeamId == Constants.PlayerTeam;

    public PlayerInput(Battle battle, HexGrid grid, GridNavigation nav)
    {
        this.battle = battle;
        this.grid = grid;
        this.gridNav = nav;

        plane = new Plane(Vector3.up, 0f);
        camera = Camera.main;
    }

    private void UpdateMouseWorldPos(ref Vector3 wPos)
    {
        var ray = camera.ScreenPointToRay(Input.mousePosition);
        if (plane.Raycast(ray, out var distance))
        {
            wPos = ray.GetPoint(distance);
        }
    }

    public void Tick()
    {
        if (!IsActive || battle.IsExecuting)
            return;

        UpdateMouseWorldPos(ref lastHitPos);

        var cell = grid.WorldPosToCell(lastHitPos);
        var isNewCell = lastCell != cell;
        lastCell = cell;

        if (isNewCell)
            UpdateNavPathToCurrent(cell);

        if (Input.GetMouseButtonDown(0) && grid.ContainsPoint(lastHitPos))
        {
            if (battle.TryGetBattlerAt(cell, out var battler))
            {
                HandleHitBattler(battler, cell);
            }
            else
            {
                HandleHitGround();
            }
        }
    }

    private void UpdateNavPathToCurrent(Vector2Int cell)
    {
        var pathStart = grid.WorldPosToCellIndex(battle.CurrentBattler.Position);
        var pathEnd = grid.CellToIndex(cell);
        if (grid.ValidateCell(cell))
            gridNav.GetPath(pathStart, pathEnd);
    }

    private void HandleHitGround()
    {
        if (!battle.CheckCanReachDest(battle.CurrentBattler, gridNav.LastPath))
            return;
        var command = new MoveCommand(battle.CurrentBattler, gridNav.LastPath);
        battle.PushCommand(command);
    }

    private bool CheckSameTeam(Battler target)
    {
        return battle.CurrentBattler.TeamId == target.TeamId;
    }

    public void SkipCurrent()
    {
        var command = new SkipCommand(battle.CurrentBattler);
        battle.PushCommand(command);
    }

    private void HandleHitBattler(Battler target, Vector2Int cell)
    {
        if (CheckSameTeam(target))
            return;

        UpdateNavPathToCurrent(cell);
        var path = gridNav.LastPath;
        if (path.Status == NavPathStatus.PathInvalid)
            return;

        var current = battle.CurrentBattler;

        if (battle.CheckCanAttackMelee(current, target))
        {
            var command = new AttackCommand(current, target);
            battle.PushCommand(command);
        }
        else
        {
            if (!battle.CheckCanReachDest(battle.CurrentBattler, path))
                return;

            var set = new CommandSet();
            set.Append(new MoveCommand(current, path));
            set.Append(new AttackCommand(current, target));

            if (current.Properties.StrikeAndReturn)
            {
                var reversed = gridNav.GetPathReversed(path);
                set.Append(new MoveCommand(current, reversed));
            }
            battle.PushCommand(set);
        }
    }
}