using UnityEngine;

public class BattlerView : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer mRenderer;

    private Vector3[] path;
    private bool moving;
    private int cornerIndex;
    private float moveSpeed;

    public Vector3 PositionFlat()
    {
        var flatPos = transform.position;
        flatPos.y = 0f;
        return flatPos;
    }
    
    public void UpdateTransform()
    {
        if (!moving)
            return;

        var target = NextPosition(Constants.MoveNextPosThresh);
        LookAtTarget(target);
        var movement = transform.forward * Time.deltaTime * moveSpeed;
        transform.position += movement;
    }

    private void LookAtTarget(Vector3 target)
    {
        var lookDir = target - transform.position;
        lookDir.y = 0f;
        transform.rotation = Quaternion.LookRotation(lookDir);
    }

    private Vector3 NextPosition(float nextDstThresh)
    {
        var cornerPos = path[cornerIndex];
        var battlerPos = transform.position;
        battlerPos.y = 0f;
        if (Vector3.Distance(battlerPos, cornerPos) < nextDstThresh)
        {
            if (cornerIndex < path.Length - 1)
            {
                cornerIndex++;
                cornerPos = path[cornerIndex];
            }
        }
        return cornerPos;
    }

    public void FinishMovement()
    {
        moving = false;
    }

    public void StartMovement(Vector3[] path)
    {
        this.path = path;
        moving = true;
        cornerIndex = 0;
    }

    public void Initialize(int teamId, float speed)
    {
        UpdateTeamView(teamId);
        this.moveSpeed = speed;
    }

    private void UpdateTeamView(int teamId)
    {
        mRenderer.material = GetTeamMaterial(teamId);
    }

    private Material GetTeamMaterial(int teamId)
    {
        var mat = new Material(Shader.Find("Standard"));
        mat.color = teamId == 0 ? Color.blue : Color.red;
        return mat;
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
