﻿using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [SerializeField]
    private HexGrid grid;

    [SerializeField]
    private GameObject cellPrefab;

    [SerializeField]
    private GridNavigation gridNav;

    [SerializeField]
    private Spawner spawner;

    public override void InstallBindings()
    {
        Container.BindInstance(grid).AsSingle();
        Container.BindInstance(cellPrefab).AsSingle();
        Container.BindInstance(gridNav).AsSingle();
        Container.BindInstance(spawner).AsSingle();
        Container.BindInterfacesAndSelfTo<Battle>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<BattleFX>().AsSingle().NonLazy();
        Container.Bind<ILogger>().To<BattleLogger>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<CellSelectionView>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerInput>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<AIInput>().AsSingle().NonLazy();
        Container.Bind<MainUIView>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
        Container.Bind<PlayerUIView>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
        Container.Bind<LogUIView>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
    }
}