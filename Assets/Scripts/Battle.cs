using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

public class Battle : ITickable, IInitializable
{
    public event Action OnInitilized;
    public event Action<Vector2Int> OnFinished;
    public event Action<Battler> OnNextBattler;

    private Battler currentBattler;
    private List<Battler> battlers;
    private IEnumerable<Battler> activeBattlers;
    private int currentIndex;
    private bool isExecuting;
    private bool finished;
    private Vector2Int teamCounters;
    private Spawner spawner;
    private HexGrid grid;
    private GridNavigation gridNav;

    public bool IsExecuting => isExecuting;
    public bool Finished => finished;
    public Battler CurrentBattler => currentBattler;
    public List<Battler> Battlers => battlers;
    public HexGrid Grid => grid;

    public void Initialize()
    {
        Start();
    }

    public Battle(HexGrid grid, GridNavigation gridNav, Spawner spawner)
    {
        this.grid = grid;
        this.gridNav = gridNav;
        this.spawner = spawner;
    }

    public void Start()
    {
        finished = false;
        isExecuting = false;
        battlers = spawner.SpawnTeamBattlers(grid, out teamCounters);
        foreach (var battler in battlers)
        {
            battler.OnDone += BattlerOnDone;
            battler.OnDied += BattlerOnDied;
            battler.OnDestroy += BattlerOnDestroy;
        }
        currentIndex = -1;
        NextBattler();
        OnInitilized?.Invoke();
    }

    private void BattlerOnDestroy(Battler battler)
    {
        battler.OnDone -= BattlerOnDone;
        battler.OnDied -= BattlerOnDied;
        battler.OnDestroy -= BattlerOnDestroy;
    }

    private void BattlerOnDied(Battler battler)
    {
        teamCounters[battler.TeamId]--;
        battler.Destroy();
        battlers[battlers.IndexOf(battler)] = null;
        activeBattlers = GetActiveBattlers();
        CheckBattleFinished();
    }
    public void Clear()
    {
        foreach (var battler in GetActiveBattlers())
        {
            battler.Destroy();
        }
        battlers.Clear();
    }

    private void CheckBattleFinished()
    {
        finished = CheckTeamWin();
        if (finished)
            OnFinished?.Invoke(teamCounters);
    }

    private void BattlerOnDone(Battler battler)
    {
        isExecuting = false;
        NextBattler();
    }

    private IEnumerable<Battler> GetActiveBattlers()
    {
        return battlers.Where(b => b != null);
    }

    private IEnumerable<int> GetActiveBattlersExceptCurrent()
    {
        return GetActiveBattlers()
            .Where(b => b.Id != currentBattler.Id)
            .Select(b => grid.WorldPosToCellIndex(b.Position));
    }

    private void NextBattler()
    {
        currentIndex = (currentIndex + 1) % battlers.Count;
        if (currentIndex == 0)
            NextRound();
        currentBattler = battlers[currentIndex];
        if (currentBattler == null)
        {
            NextBattler();
            return;
        }

        gridNav.ClearNodes();
        gridNav.DisableTargetNodes(GetActiveBattlersExceptCurrent());
        currentBattler.Activate();
        OnNextBattler?.Invoke(currentBattler);
    }

    private void NextRound()
    {
        activeBattlers = GetActiveBattlers();
        battlers = activeBattlers.ToList();
    }

    public void Tick()
    {
        if (finished || currentBattler == null)
            return;
        currentBattler.Execute();
    }

    private bool CheckTeamWin()
    {
        return teamCounters.x == 0 || teamCounters.y == 0;
    }

    public bool TryGetBattlerAt(Vector3 target, out Battler battler)
    {
        var cell = grid.WorldPosToCell(target);
        return TryGetBattlerAt(cell, out battler);
    }

    public bool TryGetBattlerAt(Vector2Int target, out Battler battler)
    {
        foreach (var active in activeBattlers)
        {
            var cell = grid.WorldPosToCell(active.Position);
            if (target == cell)
            {
                battler = active;
                return true;
            }
        }
        battler = null;
        return false;
    }

    public IEnumerable<Battler> GetTeam(int teamId)
    {
        return GetActiveBattlers().Where(b => b.TeamId == teamId);
    }

    public void PushCommand(ICommand command)
    {
        isExecuting = true;
        currentBattler.AssignCommand(command);
    }

    public bool CheckCanReachDest(Battler battler, INavPath path)
    {
        if (path == null)
            return false;
        return path.Corners.Length <= battler.Properties.MaxSteps;
    }

    public bool CheckCanAttackMelee(Vector3 sourcePos, Vector3 targetPos)
    {
        return grid.AreNeighbors(sourcePos, targetPos);
    }

    public bool CheckCanAttackMelee(Battler source, Battler target)
    {
        return grid.AreNeighbors(source.Position, target.Position);
    }
}
