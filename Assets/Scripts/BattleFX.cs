using UnityEngine;

public class BattleFX
{
    private Battle battle;
    private ParticleSystem attackFX;
    private readonly string attackFxPrefPath = "FX/AttackFX";

    public BattleFX(Battle battle)
    {
        this.battle = battle;
        battle.OnInitilized += BattleOnInitilized;
        var attackFxPref = Resources.Load<ParticleSystem>(attackFxPrefPath);
        attackFX = Object.Instantiate(attackFxPref);
    }

    private void BattleOnInitilized()
    {
        foreach (var battler in battle.Battlers)
        {
            SetBattlerSubscription(battler, true);
        }
    }

    private void SetBattlerSubscription(Battler battler, bool subscribe)
    {
        if (subscribe)
        {
            battler.OnAttack += BattlerOnAttack;
            battler.OnDestroy += BattlerOnDestroy;
        }
        else
        {
            battler.OnAttack -= BattlerOnAttack;
            battler.OnDestroy -= BattlerOnDestroy;
        }
    }

    private void BattlerOnDestroy(Battler battler)
    {
        SetBattlerSubscription(battler, false);
    }

    private void BattlerOnAttack(Battler battler, Battler target)
    {
        attackFX.transform.position = target.Position + Vector3.up;
        attackFX.Play();
    }
}
