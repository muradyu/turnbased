using UnityEngine;
using System.Collections;

public class MouseOrbit : MonoBehaviour
{
    [SerializeField]
    private float distance = 5.0f;
    [SerializeField]
    private float xSpeed = 120.0f;
    [SerializeField]
    private float ySpeed = 120.0f;
    [SerializeField]
    private float yMinLimit = -80f;
    [SerializeField]
    private float yMaxLimit = 80f;

    private float x;
    private float y;

    private readonly string mAxisX = "Mouse X";
    private readonly string mAxisY = "Mouse Y";

    void Start()
    {
        var angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;
    }

    void Update()
    {
        if (!Input.GetMouseButton(1))
            return;

        var dt = Time.deltaTime;
        x += Input.GetAxis(mAxisX) * xSpeed * dt;
        y -= Input.GetAxis(mAxisY) * ySpeed * dt;

        y = ClampAngle(y, yMinLimit, yMaxLimit);

        var rotation = Quaternion.Euler(y, x, 0);
        var negDistance = new Vector3(0.0f, 0.0f, -distance);
        var position = rotation * negDistance;
        transform.SetPositionAndRotation(position, rotation);
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360f)
            angle += 360f;
        if (angle > 360f)
            angle -= 360f;
        return Mathf.Clamp(angle, min, max);
    }
}
