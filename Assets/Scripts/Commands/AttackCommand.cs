public class AttackCommand : ICommand
{
    private bool isDone;
    private Battler battler;
    private Battler target;

    public bool IsDone => isDone;

    public AttackCommand(Battler source, Battler target)
    {
        this.battler = source;
        this.target = target;
    }

    public void Start() { }

    public void Execute()
    {
        battler.Attack(target);
        isDone = true;
    }
}
