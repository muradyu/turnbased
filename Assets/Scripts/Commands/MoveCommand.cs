public class MoveCommand : ICommand
{
    private Battler battler;
    private INavPath path;

    public bool IsDone => CheckDestinationReached();

    public MoveCommand(Battler source, INavPath path)
    {
        this.battler = source;
        this.path = path;
    }

    private bool CheckDestinationReached()
    {
        var dst = (path.Corners[path.Corners.Length - 1] - battler.Position).magnitude;
        var reached = dst <= Constants.MoveDstThresh;
        return reached;
    }

    public void Start()
    {
        battler.MoveTo(path.Corners);
    }

    public void Execute()
    {
        battler.UpdateView();
    }
}