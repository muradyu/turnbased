public class SkipCommand : ICommand
{
    private bool isDone;
    private Battler battler;

    public bool IsDone => isDone;

    public SkipCommand(Battler source)
    {
        this.battler = source;
    }

    public void Start() { }

    public void Execute()
    {
        isDone = true;
    }
}
