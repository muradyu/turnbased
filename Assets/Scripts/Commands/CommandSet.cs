using System.Collections.Generic;

public class CommandSet : ICommand
{
    private bool isDone;
    private List<ICommand> commands;
    private int currentIndex;
    public bool IsDone => isDone;

    public CommandSet(List<ICommand> commands)
    {
        this.commands = commands;
    }

    public CommandSet()
    {
        this.commands = new List<ICommand>();
    }

    public void Append(ICommand command)
    {
        this.commands.Add(command);
    }

    public void Execute()
    {
        if (commands == null || commands.Count == 0)
            return;
        if (commands[currentIndex].IsDone)
        {
            currentIndex++;
            if (currentIndex >= commands.Count)
            {
                isDone = true;
                return;
            }
            commands[currentIndex].Start();
        }
        commands[currentIndex].Execute();
    }

    public void Start()
    {
        commands[currentIndex].Start();
    }
}