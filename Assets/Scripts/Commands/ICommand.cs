public interface ICommand
{
    void Start();
    void Execute();
    bool IsDone { get; }
}