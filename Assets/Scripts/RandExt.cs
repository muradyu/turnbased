using UnityEngine;

public class RandomExt
{
    public static int RandomSign()
    {
        return Random.Range(0, 2) * 2 - 1;
    }

    public static bool RandomBool()
    {
        return Random.Range(0, 2) != 0;
    }
}
