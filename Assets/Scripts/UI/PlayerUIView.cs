using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

public class PlayerUIView : MonoBehaviour
{
    private const float buttonWidth = 80;
    private const float buttonHeight = 30;

    private PlayerInput input;
    private Battle battle;

    [Inject]
    public void Construct(PlayerInput input, Battle battle)
    {
        this.input = input;
        this.battle = battle;
    }

    private void OnGUI()
    {
        DoRestartButton();
        if (!input.IsActive)
            return;
        DoSkipButton();
    }

    private void DoSkipButton()
    {
        var skipRect = GetSkipButtonRect();
        if (GUI.Button(skipRect, "Skip"))
        {
            input.SkipCurrent();
        }
    }

    private void DoRestartButton()
    {
        var restartRect = GetRestartButtonRect();
        if (GUI.Button(restartRect, "Restart"))
        {
            battle.Clear();
            battle.Start();
        }
    }

    private Rect GetRestartButtonRect()
    {
        return new Rect(
        Screen.width - buttonWidth,
        Screen.height - buttonHeight,
        buttonWidth,
        buttonHeight);
    }

    private Rect GetSkipButtonRect()
    {
        return new Rect(
        Screen.width * 0.5f - buttonWidth * 0.5f,
        Screen.height - buttonHeight,
        buttonWidth,
        buttonHeight);
    }
}
