using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

public class MainUIView : MonoBehaviour
{
    private const float resultWWidth = 200;
    private const float resultWHeight = 40;

    private Battle battle;
    private Vector2Int teamScores;

    private const string victoryMsg = "Victory";
    private const string defeatMsg = "Defeat";

    [Inject]
    public void Construct(Battle battle)
    {
        this.battle = battle;
        battle.OnFinished += BattleOnFinished;
    }

    private void BattleOnFinished(Vector2Int teamScores)
    {
        this.teamScores = teamScores;
    }

    private void OnGUI()
    {
        if (battle.Finished)
        {
            GUILayout.Window(
                0,
                GetResultWindowRect(),
                DoResultWindow,
                GetResultMsg());
        }
    }

    private Rect GetResultWindowRect()
    {
        return new Rect(
          Screen.width * 0.5f - resultWWidth * 0.5f,
          Screen.height * 0.5f - resultWHeight * 0.5f,
          resultWWidth,
          resultWHeight);
    }

    private string GetResultMsg()
    {
        var resultMsg = teamScores.x > teamScores.y ? victoryMsg : defeatMsg;
        return resultMsg;
    }

    void DoResultWindow(int windowID)
    {
        if (GUILayout.Button("Restart"))
        {
            battle.Clear();
            battle.Start();
        }
    }
}
