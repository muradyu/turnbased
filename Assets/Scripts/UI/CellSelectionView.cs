using UnityEngine;
using Zenject;

public class CellSelectionView : ITickable, IInitializable
{
    private GameObject cellPrefab;
    private GameObject cellTarget;
    private MeshRenderer cellTargetRenderer;
    private GameObject cellCurrent;

    private HexGrid grid;
    private GridNavigation gridNav;
    private Battle battle;
    private PlayerInput playerInput;

    private const float yOffset = 0.01f;
    private readonly Vector3 vOffset = Vector3.up * yOffset;

    public CellSelectionView(GameObject cellPrefab, HexGrid grid, GridNavigation gridNav, Battle battle, PlayerInput input)
    {
        this.cellPrefab = cellPrefab;
        this.battle = battle;
        this.grid = grid;
        this.gridNav = gridNav;
        this.playerInput = input;
    }

    private void SetCellViewColor(MeshRenderer renderer, Color color)
    {
        renderer.material.color = color;
    }

    public void Tick()
    {
        SetPlayerSelectionVisibility(!battle.Finished && playerInput.IsActive);
        SetCellViewColor(cellTargetRenderer, battle.CheckCanReachDest(battle.CurrentBattler, gridNav.LastPath) ? Color.green : Color.red);
        cellCurrent.transform.position = grid.SnapWorldPos(battle.CurrentBattler.Position) + vOffset;
        cellTarget.transform.position = grid.SnapWorldPos(playerInput.LastHitPos) + vOffset;
    }

    private void SetPlayerSelectionVisibility(bool visible)
    {
        cellTargetRenderer.enabled = visible;
    }

    public void Initialize()
    {
        cellTarget = Object.Instantiate(cellPrefab);
        cellCurrent = Object.Instantiate(cellPrefab);
        cellTargetRenderer = cellTarget.GetComponent<MeshRenderer>();
        var cellCurrentRenderer = cellCurrent.GetComponent<MeshRenderer>();

        SetCellViewColor(cellTargetRenderer, Color.red);
        SetCellViewColor(cellCurrentRenderer, Color.white);
    }
}
