using System.Text;
using UnityEngine;
using Zenject;

public class LogUIView : MonoBehaviour
{
    private ILogger logger;
    private Vector2 scrollPos;
    private StringBuilder strBuilder;
    private string log;
    private const int ViewWidth = 400;
    private const int ViewHeight = 150;
    private readonly Vector2 msgAddPos = new Vector2(0f, 100f);

    [Inject]
    public void Initialize(ILogger logger)
    {
        this.logger = logger;
        logger.OnMessage += LoggerOnMessage;
        logger.OnClear += LoggerOnClear;
        strBuilder = new StringBuilder();
    }

    private void LoggerOnClear()
    {
        strBuilder.Clear();
        log = string.Empty;
    }

    private void LoggerOnMessage(string message)
    {
        strBuilder.Append(message);
        log = strBuilder.ToString();
        scrollPos += msgAddPos;
    }

    private void OnGUI()
    {
        scrollPos = GUILayout.BeginScrollView(scrollPos, GUILayout.Width(ViewWidth), GUILayout.Height(ViewHeight));
        GUILayout.Label(log);
        GUILayout.EndScrollView();
    }
}
