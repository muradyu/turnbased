using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(GridBuilder))]
public class GridBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var builder = (GridBuilder)target;
        DrawDefaultInspector();
        if (GUILayout.Button("Build Hex grid"))
        {
            builder.BuildHexGrid();
            Undo.RegisterCompleteObjectUndo(builder, "Build Hex Grid");
        }
        if (GUILayout.Button("Build Grid view"))
        {
            ClearGridView(builder);
            Undo.IncrementCurrentGroup();
            Undo.SetCurrentGroupName("Build Grid view");
            var groupIndex = Undo.GetCurrentGroup();
            BuildGridView(builder);
            Undo.CollapseUndoOperations(groupIndex);
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
    }

    private void ClearGridView(GridBuilder builder)
    {
        var t = builder.transform;
        if (t.childCount == 0)
            return;
        for (int i = t.childCount; i > 0; --i)
            DestroyImmediate(t.GetChild(0).gameObject);
    }

    public void BuildGridView(GridBuilder builder)
    {
        var grid = builder.Grid;
        var idr = Quaternion.identity;
        var parent = builder.transform;
        for (int i = 0; i < grid.Width; i++)
        {
            for (int j = 0; j < grid.Height; j++)
            {
                var instance = PrefabUtility.InstantiatePrefab(builder.CellPrefab, parent) as GameObject;
                var t = instance.transform;
                var pos = grid.CellToWorldPos(new Vector2Int(i, j));
                t.SetPositionAndRotation(pos, idr);
                Undo.RegisterCreatedObjectUndo(instance, "Create cell instance");
            }
        }
    }
}
