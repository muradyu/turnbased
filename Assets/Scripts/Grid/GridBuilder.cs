using UnityEngine;

[ExecuteInEditMode]
public class GridBuilder : MonoBehaviour
{
    [SerializeField]
    private int width;
    [SerializeField]
    private int height;
    [SerializeField]
    private float cellRad;
    [SerializeField]
    private GameObject cellPrefab;
    [SerializeField]
    private HexGrid grid;

    public int Width => width;
    public int Height => height;
    public float CellRad => cellRad;
    public GameObject CellPrefab => cellPrefab;
    public HexGrid Grid => grid;

    public void BuildHexGrid()
    {
        grid = gameObject.GetOrAddComponent<HexGrid>();
        grid.Initialize(width, height, cellRad);
    }
}
