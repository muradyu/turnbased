using System;
using UnityEngine;

public class HexGrid : MonoBehaviour
{
    [SerializeField]
    private int width;
    [SerializeField]
    private int height;
    [SerializeField]
    private float cellRad;
    [SerializeField]
    private float wStep;
    [SerializeField]
    private float hStep;

    [SerializeField]
    private Vector2 extents;
    public Vector2 Extents => extents;
    public int Width => width;
    public int Height => height;
    public float InnerRadius => wStep * 0.5f;
    public float OuterRadius => cellRad;
    public float CellDst => wStep;

    public int MaxNeighbors => 6;

    public bool AreNeighbors(Vector3 a, Vector3 b)
    {
        return AreNeighbors(WorldPosToCell(a), WorldPosToCell(b));
    }

    public bool AreNeighbors(Vector2Int a, Vector2Int b)
    {
        // Get an offset for even and uneven rows
        var offset = 1 - (a.y % 2);
        // First check middle row neighbors, then upper and lower
        if (b.y == a.y && (b.x == a.x - 1 || b.x == a.x + 1) || Math.Abs(a.y - b.y) == 1 && (b.x == a.x + offset || b.x == a.x - 1 + offset))
            return true;
        return false;
    }

    /// <summary>
    /// Fills a predifined array with cell indices of a neighboring cells.
    /// </summary>
    /// <returns>Returns a number of neighboring cells.</returns>
    public int GetNeighborsTo(Vector2Int target, int[] neighbors)
    {
        var counter = 0;
        var offset = 1 - (target.y % 2);
        AppendValid(new Vector2Int(target.x + offset, target.y - 1), neighbors, ref counter);
        AppendValid(new Vector2Int(target.x - 1 + offset, target.y - 1), neighbors, ref counter);
        AppendValid(new Vector2Int(target.x - 1, target.y), neighbors, ref counter);
        AppendValid(new Vector2Int(target.x + 1, target.y), neighbors, ref counter);
        AppendValid(new Vector2Int(target.x + offset, target.y + 1), neighbors, ref counter);
        AppendValid(new Vector2Int(target.x - 1 + offset, target.y + 1), neighbors, ref counter);
        return counter;
    }

    private void AppendValid(Vector2Int cell, int[] neighbors, ref int counter)
    {
        if (ValidateCell(cell))
        {
            neighbors[counter] = CellToIndex(cell);
            counter++;
        }
    }

    public bool ValidateCell(Vector2Int cell)
    {
        return cell.x >= 0 && cell.x < width && cell.y >= 0 && cell.y < height;
    }

    public Vector3 SnapWorldPos(Vector3 wPos)
    {
        var cell = WorldPosToCell(wPos);
        var snapPos = CellToWorldPos(cell);
        return snapPos;
    }

    public Vector2Int WorldPosToCell(Vector3 wPos)
    {
        var y = (int)((wPos.z + extents.y) / hStep + cellRad * 0.5f);
        var isEvenRow = y % 2 == 0;
        var xoffset = !isEvenRow ? wStep * 0.5f : 0f;
        var x = (int)((wPos.x + extents.x) / wStep + xoffset);
        x = Mathf.Clamp(x, 0, width - 1);
        y = Mathf.Clamp(y, 0, height - 1);
        return new Vector2Int(x, y);
    }

    public bool ContainsPoint(Vector3 pos)
    {
        return Mathf.Abs(pos.x) <= extents.x && Mathf.Abs(pos.z) <= extents.y;
    }

    public int WorldPosToCellIndex(Vector3 wPos)
    {
        var cellXY = WorldPosToCell(wPos);
        return CellToIndex(cellXY);
    }

    public int CellToIndex(Vector2Int cell)
    {
        return cell.y * height + cell.x;
    }

    public Vector2Int IndexToCell(int index)
    {
        var y = index / height;
        var x = index % width;
        return new Vector2Int(x, y);
    }

    public Vector3 IndexToWorldPos(int index)
    {
        var y = index / height;
        var x = index % width;
        return CellToWorldPos(new Vector2Int(x, y));
    }

    public Vector3 CellToWorldPos(Vector2Int cell)
    {
        var isEvenRow = cell.y % 2 == 0;
        var xoffset = isEvenRow ? wStep * 0.5f : 0f;
        var xpos = wStep * cell.x + xoffset - extents.x;
        var zpos = hStep * cell.y - extents.y;
        return new Vector3(xpos, 0f, zpos);
    }

    public void Initialize(int width, int height, float r)
    {
        this.width = width;
        this.height = height;
        cellRad = r;
        wStep = Mathf.Sqrt(3f) * r;
        hStep = r * 0.5f + r;

        var gridWh = (wStep * width - wStep * 0.5f) * 0.5f;
        var gridHh = hStep * height * 0.5f - hStep * 0.5f;

        extents = new Vector2(gridWh, gridHh);
    }
}