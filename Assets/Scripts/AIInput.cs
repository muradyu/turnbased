using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AIInput
{
    private Battle battle;
    private HexGrid grid;
    private GridNavigation gridNav;
    private AITask[] tasks;
    private IEnumerable<Battler> enemies;

    public Battle Battle => battle;
    public GridNavigation GridNav => gridNav;
    public HexGrid Grid => grid;
    public bool IsActive => battle.CurrentBattler.TeamId == Constants.AITeam;
    public IEnumerable<Battler> Enemies => enemies;

    private readonly string AITasksPath = "AI/Tasks";

    public AIInput(Battle battle, HexGrid grid, GridNavigation nav)
    {
        this.battle = battle;
        this.grid = grid;
        this.gridNav = nav;

        battle.OnNextBattler += BattleOnNextBattler;
        LoadTasks();
    }

    private void LoadTasks()
    {
        var taskAssets = Resources.LoadAll<AITask>(AITasksPath);
        tasks = new AITask[taskAssets.Length];
        for (int i = 0; i < tasks.Length; i++)
        {
            var task = Object.Instantiate(taskAssets[i]);
            task.Construct(this);
            tasks[i] = task;
        }
    }

    private void BattleOnNextBattler(Battler battler)
    {
        if (!IsActive)
            return;
        
        enemies = battle.GetTeam(Constants.PlayerTeam);

        for (int i = 0; i < tasks.Length; i++)
        {
            tasks[i].Initialize(battler);
        }
        var command = GetCommand();
        if(command == null)
            command = new SkipCommand(battler);
        battle.PushCommand(command);
    }

    private void BattlerOnDied(Battler battler)
    {
        battler.OnDied -= BattlerOnDied;
    }

    private ICommand GetCommand()
    {
        if (GetTaskByPriority(out var task))
        {
            return task.DoTask();
        }
        else return null;
    }

    private bool GetTaskByPriority(out AITask task)
    {
        AITask hpPending = null;
        for (int i = 0; i < tasks.Length; i++)
        {
            var tmpTask = tasks[i];
            if (tmpTask.CheckRunConditions())
            {
                if (hpPending == null)
                {
                    hpPending = tmpTask;
                }
                else
                {
                    if (tmpTask.Priority > hpPending.Priority)
                    {
                        hpPending = tmpTask;
                    }
                }
            }
        }
        task = hpPending;
        return task != null;
    }

    public Battler GetClosestEnemyTo(Battler src)
    {
        Battler closest = null;
        var minDst = float.MaxValue;
        foreach (var enemy in enemies)
        {
            var dst = Vector3.SqrMagnitude(src.Position - enemy.Position);
            if (dst < minDst)
            {
                minDst = dst;
                closest = enemy;
            }
        }
        return closest;
    }
}
