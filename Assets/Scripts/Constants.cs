public class Constants
{
    public const int PlayerTeam = 0;
    public const int AITeam = 1;
    public const float MoveDstThresh = 0.1f;
    public const float MoveNextPosThresh = 0.1f;
}
