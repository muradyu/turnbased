using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private BattlerView battlerPrefab;
    [SerializeField]
    private Vector2Int[] positionsTeam0;
    [SerializeField]
    private Vector2Int[] positionsTeam1;
    [SerializeField]
    private BattlerProperties defaultProps;

    public List<Battler> SpawnTeamBattlers(HexGrid grid, out Vector2Int teamCounters)
    {
        var battlers = new List<Battler>();
        teamCounters = new Vector2Int(positionsTeam0.Length, positionsTeam1.Length);
        SpawnTeam(grid, positionsTeam0, Constants.PlayerTeam, battlers);
        SpawnTeam(grid, positionsTeam1, Constants.AITeam, battlers);
        return battlers;
    }

    private void SpawnTeam(HexGrid grid, Vector2Int[] positions, int teamId, List<Battler> battlers)
    {
        var idr = Quaternion.identity;
        for (int i = 0; i < positions.Length; i++)
        {
            var snapPos = grid.CellToWorldPos(positions[i]);
            var battlerView = Instantiate(battlerPrefab, snapPos, idr);
            var properties = RandomizeDefProperties(defaultProps);
            var battler = new Battler(battlerView, battlers.Count, teamId, properties);
            battlers.Add(battler);
        }
    }

    private BattlerProperties RandomizeDefProperties(BattlerProperties defProps)
    {
        return new BattlerProperties(
            health: defProps.MaxHealth + Random.Range(0, 3),
            steps: defProps.MaxSteps + Random.Range(0, 2),
            damage: defProps.DamageAmount + Random.Range(0, 3),
            speed: defProps.MoveSpeed + Random.Range(0, 3),
            fortitude: defProps.Fortitude + Random.Range(0, 2),
            returnable: RandomExt.RandomBool());
    }
}
