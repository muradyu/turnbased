using System;
using System.Collections.Generic;
using UnityEngine;

public interface ILogger
{
    void Log(string message);
    void Clear();
    event Action<string> OnMessage;
    event Action OnClear;
}

public class BattleLogger : ILogger
{
    public event Action<string> OnMessage;
    public event Action OnClear;
    private List<string> log;
    private Battle battle;

    public BattleLogger(Battle battle)
    {
        this.battle = battle;
        battle.OnInitilized += BattleOnInitilized;
        log = new List<string>();
    }

    private void BattleOnInitilized()
    {
        Clear();
        foreach (var battler in battle.Battlers)
        {
            SetBattlerSubscription(battler, true);
        }
    }

    public void Clear()
    {
        log.Clear();
        OnClear?.Invoke();
    }

    private void SetBattlerSubscription(Battler battler, bool subscribe)
    {
        if (subscribe)
        {
            battler.OnMove += BattlerOnMove;
            battler.OnDamage += BattlerOnDamage;
            battler.OnAttack += BattlerOnAttack;
            battler.OnDied += BattlerOnDied;
            battler.OnDone += BattlerOnDone;
            battler.OnActivate += BattlerOnActivate;
            battler.OnDestroy += BattlerOnDestroy;
        }
        else
        {
            battler.OnMove -= BattlerOnMove;
            battler.OnDamage -= BattlerOnDamage;
            battler.OnAttack -= BattlerOnAttack;
            battler.OnDied -= BattlerOnDied;
            battler.OnDone -= BattlerOnDone;
            battler.OnActivate -= BattlerOnActivate;
            battler.OnDestroy -= BattlerOnDestroy;
        }
    }

    private void BattlerOnDestroy(Battler battler)
    {
        SetBattlerSubscription(battler, false);
    }

    private void BattlerOnActivate(Battler battler)
    {
        Log($"\nBattler {battler.Id} engaged");
    }

    private void BattlerOnDone(Battler battler)
    {
        Log($"\nBattler {battler.Id} finished turn");
    }

    private void BattlerOnDied(Battler battler)
    {
        Log($"\nBattler {battler.Id} died");
       
    }

    private void BattlerOnAttack(Battler battler, Battler target)
    {
        Log($"\nBattler {battler.Id} attacked {target.Id}");
    }

    private void BattlerOnDamage(Battler battler, int damage)
    {
        Log($"\nBattler {battler.Id} took {damage} damage");
    }

    private void BattlerOnMove(Battler battler, Vector3 target)
    {
        var cell = battle.Grid.WorldPosToCell(target);
        Log($"\nBattler {battler.Id} moved to {cell}");
    }

    public void Log(string message)
    {
        log.Add(message);
        OnMessage?.Invoke(message);
    }
}
