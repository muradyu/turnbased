using UnityEngine;

public class Node
{
    public float F;
    public float G;
    public float H;
    public NavNode Point;
    public Node Parent;

    public Node(){}

    public Node(NavNode p)
    {
        Point = p;
    }
    
    public void ComputeF()
    {
        F = G + H;
    }
    
    public void ComputeH(NavNode target)
    {
        H = Vector3.SqrMagnitude(target.Position - Point.Position);
    }
}
