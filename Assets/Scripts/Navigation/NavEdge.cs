﻿using System;

[Serializable]
public struct NavEdge
{
    public int NeighborId;
    public float Distance;

    public NavEdge(int neighborId, float dst)
    {
        NeighborId = neighborId;
        Distance = dst;
    }
}
