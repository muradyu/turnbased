using UnityEngine;

public enum NavPathStatus
{
    PathComplete,
    PathPartial,
    PathInvalid
}

public interface INavPath
{
    Vector3[] Corners { get; }
    NavPathStatus Status { get; }
}

public class NavPath : INavPath
{
    private Vector3[] corners;
    private NavPathStatus status;
    
    public Vector3[] Corners => corners;
    public NavPathStatus Status => status;

    public NavPath(Vector3[] corners, NavPathStatus status)
    {
        this.corners = corners;
        this.status = status;
    }
}