using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

[ExecuteInEditMode]
public class GridNavigation : MonoBehaviour
{
    [SerializeField]
    private NavNode[] nodeCache;
    [SerializeField]
    private Transform parent;
    
    [Inject]
    private HexGrid grid;
    private Dictionary<int, NavNode> nodes;
    private AStar astar;
    private INavPath lastPath;

    public INavPath LastPath => lastPath;

    public void GenerateNodes()
    {
        grid = FindObjectOfType<HexGrid>();
        if (grid == null)
        {
            Debug.LogError("Grid object cannot be found. Please create a new grid first.");
            return;
        }

        Transform container = parent;
        if (container == null)
            container = transform;

        nodeCache = new NavNode[container.childCount];
        var c = 0;
        var neighbors = new int[grid.MaxNeighbors];
        foreach (Transform ct in container)
        {
            var wPos = ct.position;
            var cell = grid.WorldPosToCell(wPos);
            var index = grid.CellToIndex(cell);
            var nbrsCount = grid.GetNeighborsTo(cell, neighbors);
            nodeCache[c] = new NavNode(index, wPos, GetEdgesTo(neighbors, nbrsCount));
            c++;
        }
    }

    private NavEdge[] GetEdgesTo(int[] neighbors, int counter)
    {
        var edges = new NavEdge[counter];
        var dst = grid.CellDst;
        for (int i = 0; i < counter; i++)
        {
            edges[i] = new NavEdge(neighbors[i], dst);
        }
        return edges;
    }

    public void ClearNodes()
    {
        foreach (var node in nodes)
        {
            node.Value.SetState(true);
        }
    }

    public void DisableTargetNodes(IEnumerable<int> targets)
    {
        foreach (var nodeId in targets)
        {
            nodes[nodeId].SetState(false);
        }
    }

    private void Awake()
    {
        nodes = GetNodes();
        astar = new AStar(nodes);
    }

    public Dictionary<int, NavNode> GetNodes()
    {
        return nodeCache.ToDictionary(n => n.NodeId);
    }

    public INavPath GetPath(Vector3 start, Vector3 end)
    {
        var startInd = grid.WorldPosToCellIndex(start);
        var endInd = grid.WorldPosToCellIndex(end);
        return GetPath(startInd, endInd);
    }

    public INavPath GetPathClamped(Vector3 start, Vector3 end, int maxSteps)
    {
        var path = GetPath(start, end);
        if (path.Corners.Length > maxSteps)
        {
            var corners = path.Corners.Take(maxSteps).ToArray();
            path = new NavPath(corners, path.Status);
        }
        return path;
    }

    public INavPath GetPath(int startInd, int destInd)
    {
        var startNode = nodes[startInd];
        var endNode = nodes[destInd];
        var status = astar.GetPath(startNode, endNode);
        var corners = astar.Path.Select(c => c.Position).ToArray();
        var path = new NavPath(corners, status);
        lastPath = path;
        return path;
    }

    public INavPath GetPathReversed(INavPath path)
    {
        var cornersReversed = new Vector3[path.Corners.Length];
        Array.Copy(path.Corners, cornersReversed, path.Corners.Length);
        Array.Reverse(cornersReversed);
        return new NavPath(cornersReversed, path.Status);
    }

    void OnDrawGizmos()
    {
        DebugDrawPath();
    }

    private void DebugDrawPath()
    {
        if (lastPath == null || lastPath.Corners.Length == 0)
            return;
        var corners = lastPath.Corners;
        Gizmos.color = Color.red;
        for (int i = 1; i < corners.Length; i++)
        {
            var dir = corners[i] - corners[i - 1];
            Gizmos.DrawRay(corners[i - 1], dir);
        }
    }
}
