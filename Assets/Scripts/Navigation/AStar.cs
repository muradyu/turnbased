using System.Collections.Generic;

public class AStar
{
    private Dictionary<int, NavNode> nodes;
    private List<NavNode> path = new List<NavNode>(pathNodes * 2);
    private Dictionary<int, Node> open = new Dictionary<int, Node>(pathNodes);
    private Dictionary<int, Node> closed = new Dictionary<int, Node>(pathNodes);

    private const int pathNodes = 128;
    private const int poolSize = 1024;
    private Node[] nodePool;
    private int poolCounter;

    public List<NavNode> Path => path;

    public AStar(Dictionary<int, NavNode> nodes)
    {
        this.nodes = nodes;
        path = new List<NavNode>(pathNodes * 2);
        open = new Dictionary<int, Node>(pathNodes);
        closed = new Dictionary<int, Node>(pathNodes);
        PreparePool();
    }

    private void PreparePool()
    {
        nodePool = new Node[poolSize];
        for (int i = 0; i < poolSize; i++)
        {
            nodePool[i] = new Node();
        }
    }

    private Node GetNode()
    {
        var node = nodePool[poolCounter];
        poolCounter++;
        return node;
    }
    private void Clear()
    {
        poolCounter = 0;
        path.Clear();
        open.Clear();
        closed.Clear();
    }

    public NavPathStatus GetPath(NavNode source, NavNode target)
    {
        Clear();
        var start = GetNode();
        start.Point = source;
        open.Add(source.NodeId, start);

        var openCounter = 1;
        Node last = null;
        var lastId = source.NodeId;
        var unreachableTarget = !target.Active;
        while (openCounter > 0)
        {
            Node current = null;
            var f = float.MaxValue;
            foreach (var node in open.Values)
            {
                if (node.F < f)
                {
                    f = node.F;
                    current = node;
                }
            }

            closed.Add(current.Point.NodeId, current);
            open.Remove(current.Point.NodeId);
            openCounter--;

            if (current.Point.NodeId == target.NodeId)
            {
                last = current;
                break;
            }

            foreach (var edge in current.Point.Edges)
            {
                if (!nodes[edge.NeighborId].Active && edge.NeighborId != target.NodeId)
                {
                    continue;
                }

                if (closed.ContainsKey(edge.NeighborId))
                    continue;

                if (!open.TryGetValue(edge.NeighborId, out var node))
                {
                    var newNode = GetNode();
                    newNode.Point = nodes[edge.NeighborId];
                    newNode.G = current.G + edge.Distance;
                    newNode.ComputeH(target);
                    newNode.ComputeF();
                    newNode.Parent = current;
                    open.Add(newNode.Point.NodeId, newNode);
                    lastId = newNode.Point.NodeId;
                    openCounter++;
                }
                else
                {
                    var newF = current.G + edge.Distance + node.H;
                    if (newF < node.F)
                    {
                        node.F = newF;
                        node.Parent = current;
                    }
                }
            }
        }

        NavPathStatus status;
        if (last == null)
        {
            status = NavPathStatus.PathInvalid;
            return status;
        }
        if (unreachableTarget)
        {
            last = last.Parent;
            status = NavPathStatus.PathPartial;
        }
        else
        {
            status = NavPathStatus.PathComplete;
        }
        path.Add(last.Point);
        while (last.Parent != null)
        {
            last = last.Parent;
            path.Add(last.Point);
        }
        path.Reverse();
        return status;
    }
}
