﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(GridNavigation))]
public class NavigationEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var nav = (GridNavigation)target;
        DrawDefaultInspector();
        if(GUILayout.Button("Generate nodes"))
        {
            nav.GenerateNodes();
            Undo.RecordObject(nav, "Build nodes");
            EditorUtility.SetDirty(nav);
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
        //if (GUILayout.Button("Test AStar"))
        //{
        //    nav.TestAStar();
        //}
    }
}
