﻿using System;
using UnityEngine;

[Serializable]
public class NavNode
{
    public NavNode(
        int id,
        Vector3 pos,
        NavEdge[] edges
        )
    {
        NodeId = id;
        Position = pos;
        Edges = edges;
        Active = true;
    }

    public int NodeId;
    public Vector3 Position;
    public NavEdge[] Edges;
    public bool Active;

    public void SetState(bool active)
    {
        Active = active;
    }
}