Shader "Unlit/hex"
{
    Properties
    {
      	_Color("Base color", Color) = (1.0, 1.0, 1.0, 1.0)
        _Size("Size", Range(0.0, 10.0)) = 1.0
        _Pow("Pow", Range(0.0, 10.0)) = 1.0
    }
    SubShader
    {
        Tags { "Queue" = "Geometry" } 

        LOD 100
        Pass
        {
            //ZWrite Off
            //Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float3 localPos : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            fixed4 _Color;
            float _Size;
            float _Pow;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.localPos = v.vertex.xyz;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed g = 1.0 - saturate(pow(dot(i.localPos, i.localPos) / _Size, _Pow));
                return _Color * g;
            }
            ENDCG
        }
    }
}
